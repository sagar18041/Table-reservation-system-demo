module ApplicationHelper
  def default_time(booking_start_time)
    time = (booking_start_time.present? ? Time.at(booking_start_time) : Time.now)
    time.strftime('%d/%m/%Y %H:%M')
  end
end
