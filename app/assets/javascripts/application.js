// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require rails-ujs
//= require turbolinks
//= require moment 
//= require fullcalendar
//= require bootstrap
//= require slimscroll
//= require fastclick
//= require adminlte
//= require demo
//= require datetimepicker
//= require reservations



  $(document).ready(function() {
    load_events()
    jQuery('.datetimepicker').datetimepicker({format:'d/m/Y H:i',step: 10});
    

    function load_events() {
      $.ajax({
        url: '/reservations.json',
        dataType: 'xml',
        data: {
          search: true
        },
        success: function(data) {
          alert('hi')
          $('#calendar').fullCalendar({
      header: {
        left: 'prev,next today',
        center: 'title',
        right: 'agendaDay'
      },
      defaultView: 'agendaDay',
      nowIndicator: true,
      now: new Date($.now()),
      navLinks: true, // can click day/week names to navigate views
      selectable: true,
      selectHelper: true,
      select: function(start, end) {
        var title = prompt('Event Title:');
        var eventData;
        if (title) {
          eventData = {
            title: title,
            start: start,
            end: end
          };
          $('#calendar').fullCalendar('renderEvent', eventData, true); // stick? = true
        }
        $('#calendar').fullCalendar('unselect');
      },
      editable: true,
      eventLimit: true, // allow "more" link when too many events
      events: data
    });
        }
      });
    }
  });

