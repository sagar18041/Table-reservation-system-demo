$(document).ready(function(){

  $("#reservation_type").change(function(){
    select_date($("#reservation_type  option:selected").val());
  });

  $("#reservation_type").click(function(){
    select_date($("#reservation_type  option:selected").val());
  });

  function select_date(selected_type) {
    if (selected_type == "WalkIn"){
      var d = new Date();
      $("#reservation_booked_at_1i").val(d.getFullYear());
      $("#reservation_booked_at_2i").val((d.getMonth()+1));
      $("#reservation_booked_at_3i").val(d.getDate());
      $("#reservation_booked_at_4i").val(d.getHours() + 1);
      $("#reservation_booked_at_5i").val(d.getMinutes());
      $("#reservation_booked_at_1i").attr("disabled","disabled");
      $("#reservation_booked_at_2i").attr("disabled","disabled");
      $("#reservation_booked_at_3i").attr("disabled","disabled");
      $("#reservation_booked_at_4i").attr("disabled","disabled");
      $("#reservation_booked_at_5i").attr("disabled","disabled");
    }
    else{
      $("#reservation_booked_at_1i").removeAttr("disabled");
      $("#reservation_booked_at_2i").removeAttr("disabled");
      $("#reservation_booked_at_3i").removeAttr("disabled");
      $("#reservation_booked_at_4i").removeAttr("disabled");
      $("#reservation_booked_at_5i").removeAttr("disabled");
    }
  }
});
