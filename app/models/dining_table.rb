class DiningTable < ApplicationRecord
  belongs_to :section
  has_many :reservations

  validates :section_id, :no_of_seat, presence: true

  delegate :name, to: :section, prefix: true

  enum status: {occupied: 1, unoccupied: 0}

  #get available table for the given time
  def self.available_table(start_time: Time.now, end_time: (Time.now + 2.hours))
    # to avoid sql injection
    start_time = start_time.to_i
    end_time = end_time.to_i
    DiningTable.joins("LEFT JOIN reservations on dining_tables.id = reservations.dining_table_id 
                and (reservations.status IN (#{Reservation::OPEN_STATUS.join(',')}) and
                ((booking_start_time <= #{start_time} and booking_end_time >= #{start_time}) or (booking_start_time <= #{end_time} and booking_end_time >= #{end_time})))")
          .where("reservations.dining_table_id IS NULL")
          .group("dining_tables.id")
  end
end
