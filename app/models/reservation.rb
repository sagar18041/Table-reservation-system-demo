class Reservation < ApplicationRecord
  acts_as_taggable
  
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  TYPES = ['OnlineReservation', 'PhoneReservation', 'WalkIn']
  RESERVATION_OVER = ['finished', 'cancel', 'noshow']
  OPEN_STATUS = [0,1,2]
  CLOSED_STATUS = [3,4,5]
  belongs_to :dining_table

  enum status: {booked: 0, arrived: 1, seated: 2, finished: 3, cancel: 4, noshow: 5}

  validates :customer_name, presence: true
  validates :customer_email, allow_blank: true , format: { with: VALID_EMAIL_REGEX }
  validates :customer_mobile, allow_blank: true, :numericality => true
  validates :booking_start_time, presence: true, :numericality => true
  validates :no_of_people, presence: true, :numericality => true
  validates :type, presence: true
  validate :set_booking_end_time, on: :create
  validates :booking_end_time, presence: true, :numericality => true
  validate :booking_start_time_should_be_current_time, on: :create #less than 10 mins
  validate :booking_start_time_should_be_less_than_booking_end_time
  validate :valid_no_of_people
  validate :table_is_available, on: :create

  before_save :set_finished_booking, on: :update

  scope :booking_start_between, -> (start_time, end_time) {where("booking_start_time>= ? and booking_start_time<= ?",start_time.to_i, end_time.to_i)}
  scope :booking_end_between, -> (start_time, end_time) {where("booking_end_time>= ? and booking_end_time<= ?",start_time.to_i, end_time.to_i)}
  scope :booking_starting_after_time, -> (start_time) {where("booking_start_time>?",start_time.to_i)}
  scope :booking_ending_before_time, -> (end_time) {where("booking_end_time<?",end_time.to_i)}
  scope :open_booking, -> {where(status: OPEN_STATUS)}
  scope :closed_booking, -> {where(status: CLOSED_STATUS)}

  #validate the booking start time
  def booking_start_time_should_be_current_time
    (((Time.now - 10.minute).to_i > booking_start_time) ? self.errors.add(:booking_start_time, "should be greater than current time") : true) if booking_start_time.present?
  end

  #validate if the bboked table is currently available or not
  def table_is_available
    unless DiningTable.available_table(start_time: booking_start_time, end_time: booking_end_time).pluck(:id).include? dining_table_id 
      self.errors.add(:dining_table_id, "is not available")
    end
  end

  #validate the booking start time should be less then end time
  def booking_start_time_should_be_less_than_booking_end_time
    ((booking_start_time > booking_end_time) ? self.errors.add(:booking_start_time, "should be less than booking end time") : true) if booking_start_time.present? and booking_end_time.present?
  end

  #validate the no.of people should less than equal to table capacity
  def valid_no_of_people
    (((self.dining_table&.no_of_seat || 0) >= (self.no_of_people || 0)) ? true : self.errors.add(:no_of_people, "cannot be accomadated in this table."))
  end

  #always set the end time as current time whenever the booking is finished/canclled/no_show
  def set_finished_booking
    if self.status_changed? and (RESERVATION_OVER.include? self.status)
      self.booking_end_time = Time.now.to_i
    end
  end

  #set the end time as +2hours then the booking start time ASSUMPTION
  def set_booking_end_time
    self.booking_end_time = (Time.at(booking_start_time) + 2.hours).to_i if booking_start_time.present?
  end

  #converting booking_start_time timestamp to datetime
  def start_time
    Time.at(booking_start_time)
  end

  #converting booking_end_time timestamp to datetime
  def end_time
    Time.at(booking_end_time)
  end

  #shorthand
  def table_name
    self.dining_table_id.to_s + "-" + self.dining_table.section_name
  end

  class << self
    #TBD need to figure out to chain the scopes
    def search(params)
      return booking_starting_after_time(DateTime.parse_time_default(params['start_time'])) if params['start_time'].present?
      return booking_ending_before_time(DateTime.parse_time_default(params['end_time'])) if params['end_time'].present?
      return open_booking if params['open'] == 'true'
      return close_booking if params['close'] == 'true'
    end

  end
end
