class ReservationsController < ApplicationController

  before_action :load_available_tables, only: [:new, :edit]
  before_action :load_reservation, only: [:edit, :update, :destroy]

  def index
    if params[:search].present?
      @reservations = Reservation.search(params[:search]).page(params[:page]).per(params[:per_page])
    else
      @reservations = Reservation.order("booking_start_time desc").page(params[:page]).per(params[:per_page])
    end
    respond_to do |format|
      format.html
      format.json {render json: @reservations.to_json(only: [:id, :customer_name, :customer_email, :customer_mobile, :no_of_people, :status, :type] , methods: [:start_time, :end_time, :table_name])}
    end
  end

  def new
    @reservation = Reservation.new
  end

  def create
    @reservation = Reservation.new(reservation_params)
    if @reservation.save
      redirect_to reservations_path, notice: "Reservation added"
    else
      flash[:notice] = "Reservation not created"
      load_available_tables
      render :new
    end
  end

  def edit
  end

  def update
    if @reservation.update(reservation_params)
      redirect_to reservations_path, notice: "Reservation Updated"
    else
      flash[:notice] = "Reservation not updated"
      load_available_tables
      render :edit
    end
  end

  def show
    
  end

  def destroy
    if @reservation.destroy
      redirect_to reservations_path, notice: "Reservation entry deleted"
    else
      render :index
    end
  end

  protected

    def load_reservation
      @reservation = Reservation.includes(:dining_table).find_by(id: params[:id])
      redirect_to reservations_path, notice: "Reservation Not found" unless @reservation.present?
    end

    def load_available_tables
      @dining_tables = DiningTable.available_table
    end

    def reservation_params
      set_booking_start_time if params['reservation']['booking_start_time'].present?
      params.require(:reservation).permit(:type, :no_of_people, :booking_start_time, :booking_end_time, :dining_table_id, :customer_name, :customer_email, :customer_mobile)
    end

    def set_booking_start_time
      params['reservation']['booking_start_time'] = DateTime.parse_time_default(params['reservation']['booking_start_time']).to_i
    end

end
