class SectionsController < ApplicationController

  before_action :find_section, only: [:edit, :update, :destroy]

  def index
    @sections = Section.order("created_at DESC").page(params[:page]).per(params[:per_page])
  end

  def new
    @section = Section.new
  end

  def create
    @section = Section.new(section_params)
    if @section.save
      redirect sections_path, notice: "Section added"
    else
      render :new
    end
  end

  def edit
  end

  def update
    if @section.update(section_params)
      redirect sections_path, notice: "Section Updated"
    else
      render :edit
    end
  end

  def show
    
  end

  def destroy
    if @section.destroy
      redirect sections_path, notice: "Section entry deleted"
    else
      render :index
    end
  end

  protected
    def find_section
      @section = Section.find_by(id: params[:id])
      redirect_to sections_path, notice: "Section Not found" unless @section.present?
    end

    def section_params
      params.require(:section).permit(:name)
    end

end
