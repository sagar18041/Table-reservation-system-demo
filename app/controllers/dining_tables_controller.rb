class DiningTablesController < ApplicationController

  before_action :load_sections
  before_action :load_dining_table, only: [:edit, :update, :destroy, :show]

  def index
    @dining_tables = DiningTable.includes(:section).order("created_at DESC").page(params[:page]).per(params[:per_page])
  end

  def new
    @dining_table = DiningTable.new
  end

  def create
    @dining_table = DiningTable.new(dining_table_params)
    if @dining_table.save
      redirect_to dining_tables_path, notice: "DiningTable added"
    else
      render :new
    end
  end

  def edit
  end

  def update
    if @dining_table.update(dining_table_params)
      redirect_to dining_tables_path, notice: "DiningTable Updated"
    else
      render :edit
    end
  end

  def show
    
  end

  def destroy
    if @dining_table.destroy
      redirect_to dining_tables_path, notice: "DiningTable entry deleted"
    else
      render :index
    end
  end

  protected

    def dining_table_params
      params.require(:dining_table).permit(:section_id, :no_of_seat, :status)
    end

    def load_dining_table
      @dining_table = DiningTable.find_by(id: params[:id])
      redirect_to dining_tables_path, notice: "Dining table Not found" unless @dining_table.present?
    end

    def load_sections
      @sections = Section.all
    end

end
