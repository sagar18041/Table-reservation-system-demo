# test/integration/post_flow_test.rb

require 'test_helper'

class DiningTableFlowTest < Capybara::Rails::TestCase
  def setup
    @one = dining_tables :one
    @two = dining_tables :two
  end

  test 'dining tables index' do
    visit dining_tables_path

    assert page.has_content?(@one.section_name)
    assert page.has_content?(@two.section_name)
  end
end