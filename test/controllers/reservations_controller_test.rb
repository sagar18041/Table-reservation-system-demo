require 'test_helper'

class ReservationsControllerTest < ActionDispatch::IntegrationTest
  test "should reserve table" do
    reservation = {reservation: {booking_start_time: DateTime.now.strftime("%d/%m/%Y %H:%M"), 
                                customer_name: 'abc', customer_email: 'abc@gmail.com', customer_mobile: '1234567890',
                                dining_table_id: 1, no_of_people: 2, type: 'WalkIn'}
                  }
    post reservations_url, params: reservation
    assert_redirected_to reservations_path
    assert_equal 'Reservation added', flash[:notice]
  end

  test "should not reserve table for incomplete details" do
    reservation = {reservation: {booking_start_time: DateTime.now.strftime("%d/%m/%Y %H:%M"), 
                                customer_name: 'abc', customer_email: 'abc@gmail.com', customer_mobile: '1234567890',
                                dining_table_id: 1,  type: 'WalkIn'}
                  }
    post reservations_url, params: reservation
    assert_equal 'Reservation not created', flash[:notice]
    assert_response 200
  end

  test "should update reservation" do
    reservation = {reservation: {booking_start_time: DateTime.now.strftime("%d/%m/%Y %H:%M"), 
                                customer_name: 'abc', customer_email: 'abc@gmail.com', customer_mobile: '1234567890',
                                dining_table_id: 1, no_of_people: 2,  type: 'WalkIn'}
                  }
    post reservations_url, params: reservation

    put reservation_url(Reservation.last), params: {reservation: {no_of_people: 3}}

    assert_equal Reservation.last.no_of_people, 3
  end
end
