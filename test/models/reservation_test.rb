require 'test_helper'

class ReservationTest < ActiveSupport::TestCase

  def setup
    @section = sections :one
    @table = dining_tables :one
  end

  test "without basic necessary details should not be able to reserve" do
    reservation = Reservation.new
    reservation.save
    assert_equal reservation.errors.full_messages, ["Dining table must exist",
                                                   "Customer name can't be blank",
                                                   "Booking start time can't be blank",
                                                   "Booking start time is not a number",
                                                   "No of people can't be blank",
                                                   "No of people is not a number",
                                                   "Type can't be blank", 
                                                   "Booking end time can't be blank",
                                                   "Booking end time is not a number", 
                                                   "Dining table is not available"]
  end

  test "should not reserve table with old time" do
    reservation = Reservation.new(booking_start_time: (Time.now - 11.minute).to_i,customer_name: "Sagar Patil", customer_mobile: "09969158990", type: "OnlineReservation", customer_email: "sagar.patil.smp@gmail.com", dining_table_id: @table.id, no_of_people: 2 )
    reservation.save

    assert_equal reservation.errors.full_messages, ["Booking start time should be greater than current time"]
  end

  test "should not reserve table with no reservation type" do
    reservation = Reservation.new(booking_start_time: (Time.now).to_i,customer_name: "Sagar Patil", customer_mobile: "09969158990",  customer_email: "sagar.patil.smp@gmail.com", dining_table_id: @table.id, no_of_people: 2 )
    reservation.save

    assert_equal reservation.errors.full_messages, ["Type can't be blank"]
  end  

  test "should reserve table with all the details" do
    reservation = Reservation.new(booking_start_time: (Time.now).to_i,customer_name: "Sagar Patil", customer_mobile: "09969158990",  customer_email: "sagar.patil.smp@gmail.com", dining_table_id: @table.id, no_of_people: 2, type: "OnlineReservation" )
    assert reservation.save
  end  

  test "should not reserve same table twice" do
    reservation1 = Reservation.create(booking_start_time: (Time.now).to_i,customer_name: "Sagar Patil", customer_mobile: "09969158990",  customer_email: "sagar.patil.smp@gmail.com", dining_table_id: @table.id, no_of_people: 2, type: "OnlineReservation" )
    
    reservation2 = Reservation.new(booking_start_time: (Time.now).to_i,customer_name: "Sagar Patil1", customer_mobile: "09969158990",  customer_email: "sagar.patil.smp@gmail.com", dining_table_id: @table.id, no_of_people: 2, type: "OnlineReservation" )
    reservation2.save
    
    assert_equal reservation2.errors.full_messages, ["Dining table is not available"]
  end 

  test "should reserve table twice after first reservation finish" do
    reservation1 = Reservation.new(booking_start_time: (Time.now).to_i,customer_name: "Sagar Patil", customer_mobile: "09969158990",  customer_email: "sagar.patil.smp@gmail.com", dining_table_id: @table.id, no_of_people: 2, type: "OnlineReservation" )
    assert reservation1.save

    reservation1.arrived!
    reservation1.seated!
    reservation1.finished!

    reservation2 = Reservation.new(booking_start_time: (Time.now).to_i,customer_name: "Sagar Patil", customer_mobile: "09969158990",  customer_email: "sagar.patil.smp@gmail.com", dining_table_id: @table.id, no_of_people: 2, type: "OnlineReservation" )
    assert reservation2.save
  end 

  test "should reserve table twice after first reservation cancelled" do
    reservation1 = Reservation.new(booking_start_time: (Time.now).to_i,customer_name: "Sagar Patil", customer_mobile: "09969158990",  customer_email: "sagar.patil.smp@gmail.com", dining_table_id: @table.id, no_of_people: 2, type: "OnlineReservation" )
    assert reservation1.save

    reservation1.cancel!

    reservation2 = Reservation.new(booking_start_time: (Time.now).to_i,customer_name: "Sagar Patil", customer_mobile: "09969158990",  customer_email: "sagar.patil.smp@gmail.com", dining_table_id: @table.id, no_of_people: 2, type: "OnlineReservation" )
    assert reservation2.save
  end   
end
