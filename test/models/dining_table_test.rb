require 'test_helper'

class DiningTableTest < ActiveSupport::TestCase

  test "should not add dinning table without section" do
    dining_table = DiningTable.new(no_of_seat: 2)
    assert  !dining_table.save, "Table added without the section."
  end

  test "should  add dinning table with section" do
    section = Section.create(name: 'indoor')
    dining_table = DiningTable.new(no_of_seat: 2, section_id: section.id)
    assert dining_table.save, "Table added with the section."
  end
  
end
