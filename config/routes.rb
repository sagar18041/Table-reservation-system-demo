Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  resources :dining_tables, :sections
  resources :reservations

  root 'welcome#index'

end
