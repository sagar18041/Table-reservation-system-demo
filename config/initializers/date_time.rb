class DateTime
  class << self
    def parse_time_default(date_time)
      begin
        DateTime.parse(date_time,'%d/%m/%Y %H:%M')
      rescue Exception => e
        return nil
      end 
    end
  end
end