Summary: Build a table reservation management system for a restaurant. The application will consist of 3 primary views:

1. Settings: Manage table's and seating capacity 
- Ability to Add/Edit/Delete a Table 
- Table Attributes: 
a) Section (Which part of the restaurant the table is present in - for this task, you can have 2 default sections as: Indoor & outdoor) 
b) Number of Seats (Capacity of the Table)

2. Calendar View 
- Day & Week overview of all reservations (Similar to Google Calendar - Day & Week view) 
- Each tile on the calendar will contain the following: 
a) Customer Name / Mobile (What ever is present), Table Number, Number of People, Status etc. 
b) Button to Edit status of reservation (Status Options: Arrived, Seated, Finished, Cancel, No-Show)

3. Create Reservation (Button available on Calendar View):

a) Reservation Types: 3 Types of Reservations 
- Walk-In 
- Phone Reservation 
- Online Reservation

b) Reservation Attributes: 
- Select Date & Time (IF Walk-In, Auto-Select Current Date/Time - Non-Editable) 
- Number of Guests 
- Table Number 
- Guest Mobile Number 
- Contact Details (Name, Email etc. -> If user details not present) 
- Tags* (Select None, 1 or Multiple Tags): 
Birthday 
Anniversary 
A la carte / Buffet 
Zomato / Dineout (In case of online reservation) 
Outside Requested 
Indoor Requested 
Window Requested 
Smoking Area

Run test suite:
- bin/rails t -f 

Assumption:
- One booking would last for mininum 2 hours
- Booking start/end time is intitally stored in timestamp to easily compare/serach available table