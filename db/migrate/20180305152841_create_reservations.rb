class CreateReservations < ActiveRecord::Migration[5.1]
  def change
    create_table :reservations do |t|
      t.string :customer_name
      t.string :customer_mobile
      t.integer :dining_table_id
      t.integer :no_of_people
      t.integer :status, default: 0
      t.string :type
      t.string :customer_email
      t.integer :booking_start_time
      t.integer :booking_end_time

      t.timestamps
    end
    add_index :reservations, :dining_table_id
    add_index :reservations, :status
    add_index :reservations, :booking_start_time
    add_index :reservations, :booking_end_time
    add_index :reservations, :type
    add_index :reservations, ['dining_table_id', 'status', 'booking_start_time', 'booking_end_time'], :name => 'reservations_booking_index'
  end
end
