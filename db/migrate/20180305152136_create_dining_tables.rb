class CreateDiningTables < ActiveRecord::Migration[5.1]
  def change
    create_table :dining_tables do |t|
      t.integer :section_id
      t.integer :no_of_seat
      t.integer :status, default: 0

      t.timestamps
    end
    add_index :dining_tables, :status
  end
end
